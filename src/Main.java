import AerialVehicles.AerialVehicle;
import AerialVehicles.F15;
import AerialVehicles.KatmamTypes.Haron.Eitan;
import Entities.Coordinates;
import Kits.AttackKit;
import Kits.BdaKit;
import Kits.IntelligenceKit;
import Kits.Kit;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import enums.airCraftFlightStatusEnum;
import enums.cameraTypesEnums;
import enums.rocketTypesEnums;
import enums.sensorTypeEnums;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Kit> kits = new ArrayList<>();
        kits.add(new IntelligenceKit(sensorTypeEnums.INFRARED));
        kits.add(new AttackKit(4, rocketTypesEnums.PYTHON));
        Coordinates intelligenceCoordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        Coordinates homeBaseCoordinates = new Coordinates(31.827604665263365, 34.81739714569337);
        Eitan airPlane = new Eitan(100, airCraftFlightStatusEnum.READY, homeBaseCoordinates, kits);
        IntelligenceMission intelligenceMission = new IntelligenceMission(intelligenceCoordinates, "Ariel", airPlane,"Iraq");

        intelligenceMission.begin();
        airPlane.flyTo(intelligenceCoordinates);
        airPlane.hoverOverLocation(intelligenceCoordinates);
        intelligenceMission.executeMission();
        airPlane.land(homeBaseCoordinates);

        System.out.println();

        List<Kit> kits1 = new ArrayList<>();
        kits1.add(new AttackKit(4, rocketTypesEnums.SPICE250));
        Coordinates attackCoordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        F15 airPlane1 = new F15(100, airCraftFlightStatusEnum.READY, homeBaseCoordinates, kits1);
        AttackMission attackMission = new AttackMission(attackCoordinates, "Ze'ev Raz", airPlane1,"Tuwaitha Nuclear Research Center");

        attackMission.begin();
        airPlane.hoverOverLocation(attackCoordinates);
        attackMission.executeMission();
        airPlane.land(homeBaseCoordinates);

        System.out.println();

        List<Kit> kits2 = new ArrayList<>();
        kits2.add(new BdaKit(cameraTypesEnums.THERMAL));
        Coordinates bdaCoordinates = new Coordinates(33.2033427805222, 44.5176910795946);
        F15 airPlane2 = new F15(100, airCraftFlightStatusEnum.READY, homeBaseCoordinates, kits2);
        BdaMission bdaMission = new BdaMission(attackCoordinates, "Ilan Ramon", airPlane2,"Tuwaitha Nuclear Research Center");

        bdaMission.begin();
        airPlane.hoverOverLocation(bdaCoordinates);
        bdaMission.executeMission();
        airPlane.land(homeBaseCoordinates);

    }
}
