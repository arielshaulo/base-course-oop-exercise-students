package AerialVehicles;

import Entities.Coordinates;
import Kits.Kit;
import enums.airCraftFlightStatusEnum;

import java.util.List;

public abstract class AerialVehicle {
    public int hoursOfFlightFromLast;
    public airCraftFlightStatusEnum airCraftFlightStatus;
    public Coordinates startingCoordinates;
    public List<Kit> kits;

    public AerialVehicle(int hoursOfFlightFromLast,airCraftFlightStatusEnum airCraftFlightStatus, Coordinates startingCoordinates, List<Kit> kits){
        this.airCraftFlightStatus = airCraftFlightStatus;
        this.hoursOfFlightFromLast = hoursOfFlightFromLast;
        this.startingCoordinates = startingCoordinates;
        this.kits = kits;
    }

    public void flyTo(Coordinates destination){
        if (airCraftFlightStatus == airCraftFlightStatusEnum.READY) {
            System.out.println("fighting to " + destination.getLatitude() + destination.getLongitude());
            airCraftFlightStatus = airCraftFlightStatusEnum.IT_THE_AIR;
        }
        if (airCraftFlightStatus == airCraftFlightStatusEnum.NOT_READY) {
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination){
        System.out.println("Landing on " + destination.getLatitude() +","+ destination.getLongitude());
    }


   public abstract void check();

   public void repair(){
       hoursOfFlightFromLast = 0;
       airCraftFlightStatus = airCraftFlightStatusEnum.READY;
   }

    public Kit getKit(Class<?> c){
        for (Kit k: kits) {
            if(k.getClass() == c) {
                return k;
            }
        }
        return null;
    }
}
