package AerialVehicles;

import Entities.Coordinates;
import Kits.AttackKit;
import Kits.IntelligenceKit;
import Kits.Kit;
import enums.airCraftFlightStatusEnum;

import java.util.List;

public class F15 extends AerialVehicle {
    public AttackKit attackKit;
    public IntelligenceKit intelligenceKit;

    public F15(int hoursOfFlightFromLast, airCraftFlightStatusEnum airCraftFlightStatus, Coordinates startingCoordinates, List<Kit> kits) {
        super(hoursOfFlightFromLast, airCraftFlightStatus, startingCoordinates, kits);
    }

    @Override
    public void check() {
        if (this.hoursOfFlightFromLast >= 250) {
            repair();
        }
        if (this.hoursOfFlightFromLast < 250) {
            this.airCraftFlightStatus = airCraftFlightStatusEnum.READY;
        }
    }
}
