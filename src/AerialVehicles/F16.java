package AerialVehicles;

import Entities.Coordinates;
import Kits.AttackKit;
import Kits.BdaKit;
import Kits.Kit;
import enums.airCraftFlightStatusEnum;

import java.util.List;

public class F16 extends AerialVehicle {
    public AttackKit attackKit;
    public BdaKit bdaKit;

    public F16(int hoursOfFlightFromLast, airCraftFlightStatusEnum airCraftFlightStatus, Coordinates startingCoordinates, List<Kit> kit) {
        super(hoursOfFlightFromLast, airCraftFlightStatus, startingCoordinates, kit);
    }

    @Override
    public void check() {
        if (this.hoursOfFlightFromLast >= 250) {
            repair();
        }
        if (this.hoursOfFlightFromLast < 250) {
            this.airCraftFlightStatus = airCraftFlightStatusEnum.READY;
        }
    }
}
