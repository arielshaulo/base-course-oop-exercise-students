package AerialVehicles.KatmamTypes.Hermes;

import AerialVehicles.KatmamTypes.Katmam;
import Entities.Coordinates;
import Kits.AttackKit;
import Kits.BdaKit;
import Kits.IntelligenceKit;
import Kits.Kit;
import enums.airCraftFlightStatusEnum;

import java.util.List;

public class Zik extends Katmam {
    public IntelligenceKit intelligenceKit;
    public BdaKit bdaKit;

    public Zik(int hoursOfFlightFromLast, airCraftFlightStatusEnum airCraftFlightStatus, Coordinates startingCoordinates, List<Kit> kit) {
        super(hoursOfFlightFromLast, airCraftFlightStatus, startingCoordinates, kit);
    }
}