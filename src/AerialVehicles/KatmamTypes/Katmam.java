package AerialVehicles.KatmamTypes;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Kits.Kit;
import enums.airCraftFlightStatusEnum;

import java.util.List;

public class Katmam extends AerialVehicle {

    public Katmam(int hoursOfFlightFromLast, airCraftFlightStatusEnum airCraftFlightStatus, Coordinates startingCoordinates, List<Kit> kit) {
        super(hoursOfFlightFromLast, airCraftFlightStatus, startingCoordinates, kit);
    }

    public void hoverOverLocation(Coordinates destination) {
        this.airCraftFlightStatus = airCraftFlightStatusEnum.IT_THE_AIR;
        System.out.println("Hovering Over: " + this.startingCoordinates.getLatitude() + "," + this.startingCoordinates.getLongitude());
    }

    @Override
    public void check() {
        if (this.hoursOfFlightFromLast >= 150) {
            repair();
        }
        if (this.hoursOfFlightFromLast < 150) {
            this.airCraftFlightStatus = airCraftFlightStatusEnum.READY;
        }
    }

}
