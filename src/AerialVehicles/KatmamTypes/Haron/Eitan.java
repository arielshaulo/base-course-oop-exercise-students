package AerialVehicles.KatmamTypes.Haron;

import AerialVehicles.KatmamTypes.Katmam;
import Entities.Coordinates;
import Kits.AttackKit;
import Kits.IntelligenceKit;
import Kits.Kit;
import enums.airCraftFlightStatusEnum;

import java.util.List;

public class Eitan extends Katmam {
    public IntelligenceKit intelligenceKit;
    public AttackKit attackKit;

    public Eitan(int hoursOfFlightFromLast, airCraftFlightStatusEnum airCraftFlightStatus, Coordinates startingCoordinates, List<Kit> kit) {
        super(hoursOfFlightFromLast, airCraftFlightStatus, startingCoordinates, kit);
    }
}

