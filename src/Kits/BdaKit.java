package Kits;

import enums.cameraTypesEnums;
import enums.sensorTypeEnums;

public class BdaKit extends Kit {
    public cameraTypesEnums cameraTypes;

    public BdaKit(cameraTypesEnums cameraTypes){
        this.cameraTypes = cameraTypes;
    }
}
