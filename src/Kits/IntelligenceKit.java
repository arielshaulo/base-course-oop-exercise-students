package Kits;

import enums.sensorTypeEnums;

public class IntelligenceKit extends Kit {
    public sensorTypeEnums sensorType;
    public IntelligenceKit(sensorTypeEnums sensorType){
        this.sensorType = sensorType;
    }
}
