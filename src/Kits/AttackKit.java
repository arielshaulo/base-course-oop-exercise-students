package Kits;

import enums.rocketTypesEnums;

public class AttackKit extends Kit {
    public int numberOfRockets;
    public rocketTypesEnums rocketTypes;

    public AttackKit(int numberOfRockets,rocketTypesEnums rocketTypes){
        this.numberOfRockets = numberOfRockets;
        this.rocketTypes = rocketTypes;
    }
}
