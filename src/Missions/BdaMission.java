package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Kits.BdaKit;

public class BdaMission extends Mission {
    public String objective;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle,String objective) {
        super(destination, pilotName, aerialVehicle);
        this.objective = objective;
    }

    @Override
    public String executeMission() {
        return this.pilotName + ": " + this.aerialVehicle.getClass() + "taking pictures of suspect " + objective + "with: " + ((BdaKit) this.aerialVehicle.getKit(BdaKit.class)).cameraTypes;
    }

}
