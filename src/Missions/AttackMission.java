package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Kits.AttackKit;

public class AttackMission extends Mission {
    public String target;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String target) {
        super(destination, pilotName, aerialVehicle);
        this.target = target;
    }

    @Override
    public String executeMission() {
        String s = this.pilotName + ": " + this.aerialVehicle.getClass() + "Attacking suspect " + target + "with: " + ((AttackKit) this.aerialVehicle.getKit(AttackKit.class)).rocketTypes + "X" + ((AttackKit) this.aerialVehicle.getKit(AttackKit.class)).numberOfRockets;
        return s;
    }

}
