package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Kits.IntelligenceKit;

public class IntelligenceMission extends Mission {
    public String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String region) {
        super(destination, pilotName, aerialVehicle);
        this.region = region;
    }

    @Override
    public String executeMission() {
        String s = this.pilotName + ": " + this.aerialVehicle.getClass() + " Collecting Data in " + region + " with: sensor type " + ((IntelligenceKit) this.aerialVehicle.getKit(IntelligenceKit.class)).sensorType;
        return s;
    }

}
