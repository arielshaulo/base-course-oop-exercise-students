package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    public Coordinates destination;
    public String pilotName;
    public AerialVehicle aerialVehicle;

    public Mission(Coordinates destination,String pilotName,AerialVehicle aerialVehicle){
        this.destination = destination;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    public void begin(){
        System.out.println("Begin Mission!");
        aerialVehicle.flyTo(destination);

    }

    public void cancel(){
        System.out.println("Abort Mission!");
        aerialVehicle.land(destination);
    }

    public void finish(){
        executeMission();
        System.out.println("Finish Mission!");
        aerialVehicle.land(destination);
    }

    public abstract String executeMission();


}
